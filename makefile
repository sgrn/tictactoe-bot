CC = gcc
FLAGS = -Wall -Wextra -pedantic
OBJECTS = main.o tictactoe.o vector.o

tictactoe : $(OBJECTS) 
	$(CC) -o tictactoe $(OBJECTS)

main.o : main.c 
	$(CC) -c main.c

vector.o : vector.c vector.h
	$(CC) -c vector.c

tictactoe.o : tictactoe.c tictactoe.h
	$(CC) -c tictactoe.c

clean :
	rm tictactoe $(OBJECTS)
