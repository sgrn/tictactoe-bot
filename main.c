/**
 * \file main.c
 * \brief Main file.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 23 January 2020
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "tictactoe.h"

int main (void)
{
	tictactoe_start();
	return EXIT_FAILURE;
}
