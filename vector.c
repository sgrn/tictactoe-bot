/**
 * \file vector.c
 * \brief Contains all vector routine (OLD VERSION).
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 23 January 2020
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "vector.h"

Vector * Vector_malloc(const size_t capacity, const size_t offset)
{
	Vector * vector;
	if (offset == 0) {
		fprintf(stderr, "vector error: offset must be greater than 0.\n");
		exit(EXIT_FAILURE);
	}
	vector = malloc(sizeof(Vector));
	if (vector == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	vector->value = NULL;
	if (capacity != 0) {
		vector->value = malloc(offset * capacity);
		if (vector->value == NULL) {
			fprintf(stderr,"error : %s.\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	vector->capacity = capacity;
	vector->cardinal = 0;
	return vector;
}

Vector * Vector_realloc(Vector * const vector, const size_t capacity, const size_t offset)
{
	Vector * newVector;
	if (vector == NULL) {
		newVector = Vector_malloc(capacity, offset);
	} else if ((offset * capacity) == 0) {
		newVector = Vector_free(vector);
	} else {
		vector->value = realloc(vector->value, offset * capacity);
		if (vector->value == NULL) {
			fprintf(stderr,"error : %s.\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		vector->capacity = capacity;
		if (vector->cardinal > capacity) {
			vector->cardinal = capacity;
		}
		newVector = vector;
	}
	return newVector;
}

void * Vector_free(void * vectorAddress)
{
	Vector * vector;
	vector = vectorAddress;
	free(vector->value);
	vector->value = NULL;
	vector->cardinal = 0;
	vector->capacity = 0;
	free(vector);
	return NULL;
}

bool Vector_isempty(const Vector * const vector)
{
	return vector->cardinal == 0;
}

bool Vector_isspaceless(const Vector * const vector)
{
	return vector->capacity == 0;
}

void * Vector_at(Vector * const vector, const size_t index, const size_t offset)
{
	return ((Byte *)vector->value) + (index * offset);
}

void Vector_right_shift(Vector * const vector, const size_t offset)
{
	size_t i, j;
	Byte * previous, * cursor, curr;
	cursor = vector->value;
	previous = malloc(offset);
	if (previous == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	for (j = 0; j < offset; j++) {
		previous[j] = cursor[j];
	}
	for (i = offset; i < offset * vector->cardinal; i++, j++) {
		curr = cursor[i];
		cursor[i] = previous[j % offset];
		previous[j % offset] = curr;
	}
	free(previous);
}

void Vector_left_shift(Vector * const vector, const size_t offset)
{
	size_t i, j, cursorIndex;
	Byte * previous, * cursor, curr;
	cursor = vector->value;
	previous = malloc(offset);
	if (previous == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	for (j = 0; j < offset; j++) {
		previous[j] = cursor[(vector->cardinal - 1) * offset + j];
	}
	for (i = offset; i < vector->cardinal * offset; i += offset) {
		for (j = 0; j < offset; j++) {
			cursorIndex = ((vector->cardinal - 1) * offset) - i + j;
			curr = cursor[cursorIndex];
			cursor[cursorIndex] = previous[j];
			previous[j] = curr;
		}
	}
	free(previous);
}

bool Vector_cmp(const Vector * const vector0, const size_t offset0, const Vector * const vector1, const size_t offset1)
{
	size_t i, byteCardinal0, byteCardinal1;
	Byte * cursor0, * cursor1;
	i = 0;
	cursor0 = vector0->value;
	cursor1 = vector1->value;
	byteCardinal0 = vector0->cardinal * offset0;
	byteCardinal1 = vector1->cardinal * offset1;
	if (byteCardinal0 == byteCardinal1) {
		while ((i < byteCardinal0) && (cursor0[i] == cursor1[i])) {
			i++;
		}
	}
	return 2 * i == byteCardinal0 + byteCardinal1;
}

bool Vector_isin(const Vector * const vector, const void * const value, const size_t offset)
{
	bool isIn;
	size_t i, j;
	const Byte * vectorCursor, * valueCursor;
	vectorCursor = vector->value;
	valueCursor = value;
	isIn = false;
	i = 0;
	while ((i < ((vector->cardinal * offset) - (offset - 1))) && (! isIn)) {
		j = 0;
		while ((j < offset) && (vectorCursor[i + j] == valueCursor[j])) {
			j++;
		}
		isIn = j == offset;
		i++;
	}
	return isIn;
}


size_t Vector_count(const Vector * const vector, const void * const value, const size_t offset)
{
	size_t i, j, counter;
	const Byte * vectorCursor, * valueCursor;
	vectorCursor = vector->value;
	valueCursor = value;
	counter = 0;
	for (i = 0; i < vector->cardinal * offset; i += offset) {
		j = 0;
		while ((j < offset) && (vectorCursor[i + j] == valueCursor[j])) {
			j++;
		}
		if (j == offset) {
			counter++;
		}
	}
	return counter;
}

size_t Vector_count_overlap(const Vector * const vector, const void * const value, const size_t offset)
{
	size_t i, j, counter;
	const Byte * vectorCursor, * valueCursor;
	vectorCursor = vector->value;
	valueCursor = value;
	counter = 0;
	for (i = 0; i < ((vector->cardinal * offset) - (offset - 1)); i++) {
		j = 0;
		while ((j < offset) && (vectorCursor[i + j] == valueCursor[j])) {
			j++;
		}
		if (j == offset) {
			counter++;
		}
	}
	return counter;
}

void Vector_push_back(Vector * const vector, const void * const value, const size_t offset)
{
	if (vector->cardinal == vector->capacity) {
		Vector_realloc(vector, vector->capacity+1, offset);
	}
	memcpy(Vector_at(vector, vector->cardinal, offset), value, offset);
	vector->cardinal++;
}

void Vector_pop_back(Vector * const vector, const size_t offset)
{
	if (Vector_isspaceless(vector)) {
		fprintf(stderr, "vector error: can't pop NULL vector.\n");
		exit(EXIT_FAILURE);
	} else if (Vector_isempty(vector)) {
		Vector_realloc(vector, vector->capacity-1, offset);
	} else {
		vector->cardinal--;
	}
}

void Vector_push_front(Vector * const vector, const void * const value, const size_t offset)
{
	if (vector->cardinal == vector->capacity) {
		Vector_realloc(vector, vector->capacity+1, offset);
	}
	Vector_right_shift(vector, offset);
	memcpy(vector->value, value, offset);
	vector->cardinal++;
}

void Vector_pop_front(Vector * const vector, const size_t offset)
{
	if (Vector_isspaceless(vector)) {
		fprintf(stderr, "vector error: can't pop NULL vector.\n");
		exit(EXIT_FAILURE);
	} else if (Vector_isempty(vector)) {
		Vector_realloc(vector, vector->capacity-1, offset);
	} else {
		Vector_left_shift(vector, offset);
		vector->cardinal--;
	}
}
