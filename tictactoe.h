/*!
 * \file tictactoe.h
 * \brief Contains all the tictactoe routine headers.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 04 February 2020
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef TICTACTOE_H
	#define TICTACTOE_H

	#include <stdlib.h>
	#include <stdio.h>
	#include <time.h>

	#include "vector.h"

	typedef enum {
		EMPTY = 126,
		HUMAN = 48,
		BOT = 43
	} Cell;

	VECTOR(Cell *, Cell)
	VECTOR(VectorCell *, Grid)

	typedef struct {
		ssize_t y;
		ssize_t x;
	} Coordinate;

	VECTOR(Coordinate, Coord)
	VECTOR(size_t, SizeT)

	typedef struct {
		Coordinate gridIndex;
		ssize_t fillingLevel;
		Coordinate nextMove;
	} Segment;


	/**
	 * \fn long randint(long min, long max)
	 * \brief Returns a random integer between min and max.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param Minimum value.
	 * \param Maximum value.
	 * \return random number.
	 */

	long randint(long min, long max);

	/**
	 * \fn Cell * tictactoe_cell_allocinit(void)
	 * \brief Allocates and initializes a grid line.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \return A grid line.
	 */
	Cell * tictactoe_cell_allocinit(void);

	/**
	 * \fn VectorGrid * tictactoe_grid_allocinit(const size_t base)
	 * \brief Allocates and initializes a grid (vectors of vectors of pointers on Cell type element).
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param base Line and column grid size.
	 * \return game Grid on which the user will play.
	 */
	VectorGrid * tictactoe_grid_allocinit(const size_t base);

	/**
	 * \fn VectorGrid * tictactoe_grid_free(VectorGrid * const grid)
	 * \brief Frees memory taken by a grid.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Grid to be freed.
	 * \return Constant NULL value
	 */
	VectorGrid * tictactoe_grid_free(VectorGrid * const grid);

	/**
	 * \fn void tictactoe_grid_display(VectorGrid * const grid)
	 * \brief Displays a given grid.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Grid to display.
	 */
	void tictactoe_grid_display(VectorGrid * const grid);

	/**
	 * \fn size_t tictactoe_get_rectangular_line_index(const Coordinate coord)
	 * \brief Returns line index in the grid on which the element of given coordinate is.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param coord Coordinate of an element.
	 * \return Index of the line.
	 */
	size_t tictactoe_get_rectangular_line_index(const Coordinate coord);

	/**
	 * \fn size_t tictactoe_get_rectangular_col_index(const VectorGrid * const grid, const Coordinate coord)
	 * \brief Returns column index in the grid on which the element of given coordinate is.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid A given grid.
	 * \param coord Coordinate of an element.
	 * \return Index of the column.
	 */
	size_t tictactoe_get_rectangular_col_index(const VectorGrid * const grid, const Coordinate coord);

	/**
	 * \fn size_t tictactoe_get_rectangular_descending_diagonal_index(const VectorGrid * const grid, const Coordinate coord)
	 * \brief Returns descending diagonal index in the grid on which the element of given coordinate is.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid A given grid.
	 * \param coord Coordinate of an element.
	 * \return Index of the descending diagonal.
	 */
	size_t tictactoe_get_rectangular_descending_diagonal_index(const VectorGrid * const grid, const Coordinate coord);

	/**
	 * \fn size_t tictactoe_get_rectangular_ascending_diagonal_index(const VectorGrid * const grid, const Coordinate coord)
	 * \brief Returns ascending diagonal index in the grid on which the element of given coordinate is.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid A given grid.
	 * \param coord Coordinate of an element.
	 * \return Index of the ascending diagonal.
	 */
	size_t tictactoe_get_rectangular_ascending_diagonal_index(const VectorGrid * const grid, const Coordinate coord);

	/**
	 * \fn Coordinate tictactoe_convert_rectangular_to_square_coord(const VectorGrid * const grid, const Coordinate rectCoord)
	 * \brief Converts a given rectangular coordinate into linear coordinate.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid A given grid.
	 * \param coord Coordinate to convert.
	 * \return Plan coordinate
	 */
	Coordinate tictactoe_convert_rectangular_to_square_coord(const VectorGrid * const grid, const Coordinate rectCoord);

	/**
	 * \fn bool tictactoe_is_complete(const VectorCell * const cells, const Cell symbol)
	 * \brief Tests if a given line is full of a given symbol.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param cells Given segment.
	 * \param symbol Player symbol.
	 * \return true if a given line is full of a given symbol else false. 
	 */
	bool tictactoe_is_complete(const VectorCell * const cells, const Cell symbol);

	/**
	 * \fn bool tictactoe_isthere_winner(const VectorGrid * const grid, Coordinate lastMove)
	 * \brief Tests if the last player won the game.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \param lastMove Last move to have been played.
	 * \return true if there is a winner else false.
	 */
	bool tictactoe_isthere_winner(const VectorGrid * const grid, Coordinate lastMove);

	/**
	 * \fn Segment tictactoe_determine_segment(const VectorGrid * const grid, const size_t rectangularY, const Cell playerSymbol)
	 * \brief Constructs segment based on how much a given line, column or diagonal is filled with player symbols.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \param rectangularY Index of the given line, column or diagonal on which the segment will be constructed.
	 * \param playerSymbol Symbol of the player.
	 * \return A newly constructed segment.
	 */
	Segment tictactoe_determine_segment(const VectorGrid * const grid, const size_t rectangularY, const Cell playerSymbol);

	/**
	 * \fn Segment tictactoe_determine_optimal_orientation(const VectorGrid * grid, const Coordinate previousMove)
	 * \brief Chooses one of the lines, columns, diagonals segment that intersect at a given point. 
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \param previousMove Point of the grid at which the intersection occurs.
	 * \return Chosen line or column or diagonal
	 * The chosen segment will be the one filled with the most number of adversary player symbols.
	 */
	Segment tictactoe_determine_optimal_orientation(const VectorGrid * grid, const Coordinate previousMove);

	/**
	 * \fn size_t tictactoe_count_symbol(const VectorCell * const cells, const Cell symbol)
	 * \brief Counts symbols on a given segment.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param cells Given segment.
	 * \param symbol Given symbol to count.
	 * \return Number of symbol in a segment.
	 */
	size_t tictactoe_count_symbol(const VectorCell * const cells, const Cell symbol);

	/**
	 * \fn size_t tictactoe_determine_opponent_filling_level(const VectorGrid * const grid, const size_t rectangularY, const Coordinate placement, const Cell opponentSymbol)
	 * \brief Determines opponent's filling level of all segments (line, column, diagonals) intersecting at a given coordinate.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \param rectangularY Segment index in the grid.
	 * \param placement Coordinate at which the intersection occurs.
	 * \param opponentSymbol Player's opponent symbol.
	 * \return Opponent's fillingLevel.
	 */
	size_t tictactoe_determine_opponent_filling_level(const VectorGrid * const grid, const size_t rectangularY, const Coordinate placement, const Cell opponentSymbol);

	/**
	 * \fn void tictactoe_determine_optimal_placement(const VectorGrid * const grid, Segment * const segment, const Cell opponentSymbol, const Cell playerSymbol)
	 * \brief Determines best position to play in a chosen segment, based on how much it would block the opponent.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \param segment Best previously chosen segment.
	 * \param opponentSymbol Player's opponent symbol.
	 * \param playerSymbol Player's symbol.
	 */
	void tictactoe_determine_optimal_placement(const VectorGrid * const grid, Segment * const segment, const Cell opponentSymbol, const Cell playerSymbol);

	/**
	 * \fn Segment tictactoe_determine_fullest_segment(const VectorGrid * const grid, const VectorCoord * const history, const Cell opponentSymbol)
	 * \brief Determines segment in the grid with largest amount of current player symbol.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \param history Current player's history of previous played position.
	 * \param opponentSymbol Player's opponent symbol.
	 * \return Chosen segment.
	 */
	Segment tictactoe_determine_fullest_segment(const VectorGrid * const grid, const VectorCoord * const history, const Cell opponentSymbol);

	/**
	 * \fn bool tictactoe_are_equal_coordinate(Coordinate c0, Coordinate c1)
	 * \brief Tests equality between to coordinates.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param c0 First coordinate.
	 * \param c1 Second coordinate.
	 * \return true if the two coordinates are equal else false.
	 */
	bool tictactoe_are_equal_coordinate(Coordinate c0, Coordinate c1);

	/**
	 * \fn Coordinate tictactoe_determine_first_move(const VectorGrid * const grid)
	 * \brief Figures out what first move the bot should do.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \return Position chosen on the grid.
	 */
	Coordinate tictactoe_determine_first_move(const VectorGrid * const grid);

	/**
	 * \fn void tictactoe_grid_alter(VectorGrid * const grid, const Coordinate move, const Cell symbol)
	 * \brief Writes changes inside the grid.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Grid on which changes will occur.
	 * \param move Position on the grid at which changes will occur.
	 * \param symbol Player symbol to write.
	 */
	void tictactoe_grid_alter(VectorGrid * const grid, const Coordinate move, const Cell symbol);

	/**
	 * \fn Coordinate tictactoe_machine_play(VectorGrid * const grid, VectorCoord * const botHistory, const VectorCoord * const humanHistory)
	 * \brief Returns current bot playing position.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \param botHistory Vectors containing all previous bot played positions.
	 * \param humanHistory Vectors containing all previous human played positions.
	 * \return Playing positions.
	 */
	Coordinate tictactoe_machine_play(VectorGrid * const grid, VectorCoord * const botHistory, const VectorCoord * const humanHistory);

	/**
	 * \fn Coordinate tictactoe_get_humain_input(VectorGrid * const grid, VectorCoord * const humanHistory)
	 * \brief Returns current human playing position.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \param humanHistory Vectors containing all previous human played positions.
	 * \return Playing positions.
	 */
	Coordinate tictactoe_get_humain_input(VectorGrid * const grid, VectorCoord * const humanHistory);

	/**
	 * \fn bool tictactoe_play_as(VectorGrid * const grid, bool playerId, VectorCoord * const botHistory, VectorCoord * const humanHistory)
	 * \brief Chooses what player is going to play and executes corresponding routines.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \param grid Game grid.
	 * \param playerId Either true or false depending on who starts.
	 * \param botHistory Vectors containing all previous bot played positions.
	 * \param humanHistory Vectors containing all previous human played positions.
	 * \return true if the player won else false.
	 */
	bool tictactoe_play_as(VectorGrid * const grid, bool playerId, VectorCoord * const botHistory, VectorCoord * const humanHistory);

	/**
	 * \fn void tictactoe_print_help(void)
	 * \brief Prints help.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 */
	void tictactoe_print_help(void);

	/**
	 * \fn size_t tictactoe_get_gridlenght(void)
	 * \brief Gets grid length from the user input.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 * \return grid length.
	 */
	size_t tictactoe_get_gridlenght(void);

	/**
	 * \fn void tictactoe_start(void)
	 * \brief Starts the game.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 04 February 2020
	 */
	void tictactoe_start(void);

#endif
