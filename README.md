# CLI version of the tictactoe Game.

This project is a CLI version of the tictactoe game along with an unbeatable bot and a resizable grid.

![3 x 3 cells original grid size](Img/board.png)

![10 x 10 cells example](Img/board1.png)

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

## About

### Rules.

In order to win the game, a player must place three of their marks in a horizontal, vertical, or diagonal row.

### Unbeatable bot.

The games comes with an unbeatable bot. No matter what grid length the player chooses the outcome of the game will always either be a tie or a bot win.

### Functionalities

* No matter what grid size you choose (except for 2*2 and 1*1 cells), the outcome will either be a tie or a win for the bot.

* The first player to start moving is determined randomly.

* Your symbol is '0', the BOT symbol is '+' and an empty cell is represented by '~' symbol.

* You play by entering the Y and X coordinates of the cell in which you wanna put your symbol.

* Every time the BOT plays, you will be noticed of where the move occurred.

## How to compile and execute on GNU/Linux.

Compile.

`make`

Execute.

`./2048`
